FROM php:7.2.3-fpm-alpine3.7

RUN apk add --no-cache zlib-dev bash nano && \
    docker-php-ext-install pdo_mysql zip && \
    apk del zlib-dev

RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/b107d959a5924af895807021fcef4ffec5a76aa9/web/installer -O - -q \
	| php -- --quiet --filename=composer --install-dir=/usr/local/bin && \
    composer global require hirak/prestissimo

VOLUME /data/application
WORKDIR /data/application
